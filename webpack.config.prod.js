var path = require('path');
var webpack = require('webpack');

const PATHS = {
    app: path.join(__dirname, 'src'),
    build: path.join(__dirname, 'dist')
};

var config = {

    context: PATHS.app,

    entry: [
       PATHS.app
    ],

    output: {
        //publicPath: '/',
        path: PATHS.build,
        filename: 'gallery.js'
    },

    resolve: {
        extensions: ['', '.js', '.jsx', 'sass'],
        root: [PATHS.app]
    },

    devtool: 'cheap-module-source-map',

    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loaders: ['react-hot', 'babel'],
                exclude: /(node_modules|bower_components)/,
                include: PATHS.app
            }
        ]
    },

    plugins: [

        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
/*
        new webpack.optimize.UglifyJsPlugin({
            minimize: true,
            compress: {
                warnings: false,

                // Drop `console` statements
                drop_console: true
            }
        })
*/
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true,

            beautify: false,

            // Eliminate comments
            comments: false,

            // Compression specific options
            compress: {
                warnings: false,

                // Drop `console` statements
                drop_console: true
            },

            // Mangling specific options
            mangle: {
                // Don't mangle $
                except: ['$', 'webpackJsonp'],

                // Don't care about IE8
                screw_ie8: true,

                // Don't mangle function names
                keep_fnames: true,
            }
        })
    ]
}



module.exports = config;