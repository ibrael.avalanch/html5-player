import React, {Component} from 'react'
import {connect} from 'react-redux'
import {loadVideos} from '../actions/app'

/**
 *
 */
@connect( ({ meta, searching }) => ({ ...meta, searching: searching }))
export default class Search extends Component {

    constructor (props) {

        super(props)

        this.search = this.search.bind(this)
    }

    search (e) {
        console.log('search', e.target.value)
        let { dispatch} = this.props

        dispatch(loadVideos(e.target.value))
    }



    render () {

        let { query, searching} = this.props

        return <div className="gallery__vote">
                <div className="gallery__form">
                    <div className="gallery__input-field input-field">
                        <input autoComplete="off" id="vote" value={query}
                               className="input-text icon" type="search" onChange={this.search} name="query"
                               placeholder={Translator.trans('artist_search', {}, 'TOWAppBundle')} />
                    </div>
                    <span className="gallery__submit">
                        <button type="submit" className={ searching ? "submit-btn loader" : "submit-btn"} data-loading={searching} onClick={this.search}>
                            OK
                        </button>
                    </span>
                </div>
                <h1 className="gallery__title"><span> {Translator.trans('vote', {}, 'TOWAppBundle', 'fr')} </span>{Translator.trans('favorite_project', {}, 'TOWAppBundle', 'fr')}  </h1>

        </div>
    }

}
