import {throttle} from 'lodash'

var _requestAnimationFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame;

/**
 * Get viewport size
 * @returns {{width: number, height: number}}
 */
var getViewportSize = function() {
    return {
        width: window.document.documentElement.clientWidth,
        height: window.document.documentElement.clientHeight
    };
};


/**
 * get current scroll position
 * @returns {{x: Number, y: Number}}
 */
var getCurrentScroll = function() {
    return {
        x: window.pageXOffset,
        y: window.pageYOffset
    };
};

/**
 * Get the element’s dimensions and position
 *
 * @param elem
 * @returns {{top: number, left: number, height: number, width: number}}
 */
var getElemInfo = function(elem) {
    var offsetTop = 0;
    var offsetLeft = 0;
    var offsetHeight = elem.offsetHeight;
    var offsetWidth = elem.offsetWidth;

    do {
        if (!isNaN(elem.offsetTop)) {
            offsetTop += elem.offsetTop;
        }
        if (!isNaN(elem.offsetLeft)) {
            offsetLeft += elem.offsetLeft;
        }
    } while ((elem = elem.offsetParent) !== null);

    return {
        top: offsetTop,
        left: offsetLeft,
        height: offsetHeight,
        width: offsetWidth
    };
};


/**
 *
 * @param elem
 */
var checkVisibility = function(elem) {
    var viewportSize = getViewportSize();
    var currentScroll = getCurrentScroll();
    var elemInfo = getElemInfo(elem);
    var spaceOffset = 0.2;
    var elemHeight = elemInfo.height;
    var elemWidth = elemInfo.width;
    var elemTop = elemInfo.top;
    var elemLeft = elemInfo.left;
    var elemBottom = elemTop + elemHeight;
    var elemRight = elemLeft + elemWidth;

    var checkBoundaries = function() {
        // Defining the element boundaries and extra space offset
        var top = elemTop + elemHeight * spaceOffset;
        var left = elemLeft + elemWidth * spaceOffset;
        var bottom = elemBottom - elemHeight * spaceOffset;
        var right = elemRight - elemWidth * spaceOffset;

        // Defining the window boundaries and window offset
        var wTop = currentScroll.y + 0;
        var wLeft = currentScroll.x + 0;
        var wBottom = currentScroll.y - 0 + viewportSize.height;
        var wRight = currentScroll.x - 0 + viewportSize.width;

        // Check if the element is within boundary
        return (top < wBottom) && (bottom > wTop) && (left > wLeft) && (right < wRight);
    };

    return checkBoundaries();
};


export const onViewport = (element, handler) => {
    if(checkVisibility(element))
        handler()
}

export const spy = (element, handler) => {
    var scrollHandler = throttle(function() {


        console.log('scroll')
        if(checkVisibility(element))
            _requestAnimationFrame(handler);
    }, 300);

    var resizeHandler = throttle(function() {
        if(checkVisibility(element))
            _requestAnimationFrame(handler);

    }, 300);

    if (window.addEventListener) {
        addEventListener('scroll', scrollHandler, false);
        addEventListener('resize', resizeHandler, false);
    } else if (window.attachEvent) {
        window.attachEvent('onscroll', scrollHandler);
        window.attachEvent('onresize', resizeHandler);
    } else {
        window.onscroll = scrollHandler;
        window.onresize = resizeHandler;
    }
}