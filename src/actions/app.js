require('whatwg-fetch');

export const next = () => {

    return (dispatch, getState) => {

        let {list, playing} = getState(),
            next = (playing + 1 < list.length) ? playing + 1 : 0

        dispatch({
            type: 'VIDEO_PLAYING',
            index: next
        })

    }
}

export const prev = () => {

    return (dispatch, getState) => {

        let {list, playing} = getState(),
            prev = (playing > 0) ? playing - 1 : list.length - 1

        dispatch({
            type: 'VIDEO_PLAYING',
            index: prev
        })

    }
}



export const loadMore = () => {
    return (dispatch, getState) => {

        let {query, limit, offset, has_more} = getState().meta

        /*if(!has_more)
            return;
*/
        dispatch ({
            type: 'REQUEST_START'
        })


        return fetch(
            Routing.generate('api_project_list', { offset: offset + limit, limit, query: query === undefined ? null : query}),
            {
                credentials: 'same-origin',
                method: 'GET'
            }
        )
            .then(res => {

                dispatch({
                    type: 'REQUEST_SUCCESS'
                })

                return res.json()
            })
            .then(json => dispatch({
                type: 'APPEND_VIDEOS',
                json
            }))

    }
}

export const shareFacebook =  (index) => {

    return (dispatch, getState) => {
        var list  = getState().list;



        share(list[index].urls.absolute, function (response) {
            console.log(response)

            if(!response.error) {

                return fetch(list[index].urls.callback_share_facebook, {
                    method: 'post', withCredentials: true
                })
                    .then(res => res.json())
                    .then(video => {
                        list[index] = video

                        dispatch(updateVideo(index, list))
                    })
            }

        })

    }
}


export const vote = (index) =>  {
    
    return (dispatch, getState) => {

        var video  = getState().list[index];


        dispatch ({
            type: 'VOTE_START',
            index
        })

        return fetch(video.urls.vote, {
            method: 'post',
            withCredentials: true,
            credentials: 'same-origin'
        }).then(res => res.json())
        .then(v => {
            dispatch(updateVideo(index, v))

            dispatch({
                type: 'VOTE_SUCCESS',
                index
            })
        })
    }
}


export const updateVideo = (index, v) => {

    return (dispatch) => {

        dispatch({
            type: 'UPDATE_VIDEO',
            video: v,
            index: index
        })
    }
}

export const play = (index) => {


    return dispatch => {

        dispatch({
            type: 'VIDEO_PLAYING',
            index
        })

        return Promise.resolve(index)
    }
}


export const loadVideos = (query = null, offset = 0, limit = 12) => {

    return (dispatch) => {

        dispatch({
            type: 'REQUEST_START'
        })



        return fetch(
            Routing.generate('api_project_list', { offset, limit, query}),
            {
                credentials: 'same-origin',
                method: 'GET'
            }
        )
            .then(res => {

                dispatch({
                    type: 'REQUEST_SUCCESS'
                })

                return res.json()
            })
            .then(json => dispatch({
                type: 'LOADED_VIDEOS',
                json
            }))
    }

}