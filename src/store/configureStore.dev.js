import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../reducers';

import createLogger from 'redux-logger';

const enhancer = compose(
    applyMiddleware(thunk,  createLogger()),
    window.devToolsExtension ? window.devToolsExtension() : f => f
);

export default function configureStore(initialState = {}) {
    const store = createStore(reducers, initialState, enhancer);

    if (module.hot) {
        module.hot.accept('../reducers', () =>
            store.replaceReducer(require('../reducers').default)
        );
    }

    return store;
}