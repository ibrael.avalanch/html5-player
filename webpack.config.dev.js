var path = require('path');
var webpack = require('webpack');
const merge = require('webpack-merge');

const PATHS = {
    app: path.join(__dirname, 'src'),
    build: path.join(__dirname, 'dist'),
};

var config = {

    context: PATHS.app,


    entry: [
        'webpack-dev-server/client?http://localhost:8080',
        'webpack/hot/only-dev-server',
        PATHS.app
    ],

    output: {
        publicPath: 'http://localhost:8080/',
        path: PATHS.build,
        filename: 'gallery.js'
    },

    resolve: {
        extensions: ['', '.js', '.jsx'],
        root: [PATHS.app]
    },

    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loaders: ['react-hot', 'babel'],
                exclude: /(node_modules|bower_components)/,
                include: PATHS.app
            }
        ]
    },

    plugins: [

        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('development')
            }
        }),

        // Enable multi-pass compilation for enhanced performance
        // in larger projects. Good default.
        new webpack.HotModuleReplacementPlugin({
            multiStep: true
        }),

        new webpack.NoErrorsPlugin()
    ],

    sourceMaps: 'eval-source-map',
    devServer: {
        // Enable history API fallback so HTML5 History API based
        // routing works. This is a good default that will come
        // in handy in more complicated setups.
        historyApiFallback: true,

        // Unlike the cli flag, this doesn't set
        // HotModuleReplacementPlugin!
        hot: true,

        //inline: true,

        watch: true,
        // Display only errors to reduce the amount of output.
        stats: 'errors-only',
        noInfo: false,
        progress: true,
        colors: true,
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000 // is this the same as specifying --watch-poll?
        },

        // Parse host and port from env to allow customization.
        //
        // If you use Vagrant or Cloud9, set
        // host: options.host || '0.0.0.0';
        //
        // 0.0.0.0 is available to all network devices
        // unlike default `localhost`.
        host: '0.0.0.0', // Defaults to `localhost`
        port: process.env.PORT || 8080// Defaults to 8080
    }
}

module.exports = config;